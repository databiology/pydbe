#PyDBE

PyDBE, is a Python SDK package for Databiology for Enterprise. It can be used to
 run scripts for automation tasks.

##Install
###Requirements
1. Python 2.7
2. Python pip
3. A valid user account with assigned roles in a DBE instance.

###Installation
1. Download the python package from Project 57 - _DBE Development Project_ under the  [Comments section](https://www.lab.databiology.net/dbe/userlab/show-project.html?tab=comments&projectId=57).
The __PyDBE__ package is named `dbe_pydbe-X.Y.Z-py2-none-any.whl`, where _X.Y.Z_ is the version number.

2. Install on your computer using pip:

    pip install path/to/dbe_pydbe-XXX-py2-none-any.whl

##Using PyDBE

###Login - dbecli
Start generating an access token for a DBE instance with __dbecli__, which is a command line client.

####dbecli login
Users can login through the __dbecli__ login subcommand before using PyDBE. If you have successfully installed PyDBE, then __dbecli__ will be available on your computer also.

    Usage: dbecli login [OPTIONS]

    Login to DBE

    Options:
      -s, --showtoken      Show token through stdout after login successfully
      -l, --list           List details on current login
      -r, --refresh        Refresh the existing token
      -e, --endpoint TEXT  DBE Endpoint e.g. https://www.lab.databiology.net/dbe
      -u, --username TEXT  Login username
      -p, --password TEXT  Login password
      -t, --token          Login by token, with mask prompt
      -h, --help           Show this message and exit.


Examples:

Login using username and password for standalone environment

    $ dbecli login -e https://www.__your_instance__.databiology.net/dbe -u __your_username__ -t __your_webservice_token__
    Token filename : /home/your_username/.dbe/node.jwt
    Please paste your token content:
    Logged in DBE https://www.your_instance.databiology.net/dbe for username, and stored to /home/username/.dbe/node.jwt

Login using token for single sign on environment (e.g. Lab, or environment using SAML IDP)

    $ dbecli login -t
    Token filename : /home/__your_username__/.dbe/node.jwt
    Please paste your token content:
    Logging in DBE https://www.__your_instance__.databiology.net/dbe for __your_username__
    Logged in DBE, and stored to /home/your_username/.dbe/node.jwt

###Starting PyDBE in interactive mode
After creating a login token with DBECLI you can start a Python session to use PyDBE. For better debugging information it is recommended to use the `logging` package in your code.

    $ python
    Python 2.7.14 (default, Feb 17 2018, 09:47:19)
    [GCC 4.9.2] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import pydbe.model
    >>> import logging
    >>> logging.basicConfig()
    >>> logging.getLogger("pydbe").setLevel(logging.DEBUG)

##Basic functions
* The basic functions include operations to retrieve, create, update, or remove entities such as Projects, Workunits, Resources, Samples, Extracts, and Datasets.
* All entities can be queried but only some can be directly created or deleted.
* Each function can be accessed with their entity properties such as: name, id, projectid, workunitid, jobid, etc.
* Multiple selections are possible by adding more than one property, for example: ```pydbe.model.Workunit.get(projectid=2, userid=5)``` will return all workunits generated in Project 2 by userid 5.

###Functions
####get
Returns an list of objects, or a single object using the `_first=True` option
####get_async
Returns an iterable object, or a single object using the `_first=True` option
####get_options
Returns a list of the available elements for the entity.
####put
Creates or updates an entity. Note that the entity needs to be in “running” status. Note: Not every endpoint supports ‘put’
####put_options
Returns a list of the available elements for creation or update for the entity.
####post
Creates a new entity. Note: Not every endpoint support ‘post’
####post_options
Returns a list of the available elements for creation for the entity.

##Full documentation
Please refer to [here](https://docs.databiology.net/tiki-index.php?page=PyDBE%2BManual).
