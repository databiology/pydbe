#!/usr/bin/env python

## 06_get_resource_metadata.py
## Example PyDBE script to retrieve metadata information for a Resource.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import logging

# DBE pydbe.model package
import pydbe.model

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe").setLevel(logging.DEBUG)
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print """
        Get resource metadata, e.g. extract, sample and also customattribute/customannotation associate with them

        customattribute and customannotation are "array of dict" or "array of keyvalue pair"
            i.e. [{key1:value}, {key2:value}....]

            resource = pydbe.model.Resource.get_async(id=xxx, _first=True)
            for customattribute in resource.extract.customattribute:
                print customattribute
            for customannotation in resource.extract.customannotation:
                print customannotation

        customattributemap and customannotationmap are dict
            i.e. {key1:value, key2:value, .... }

            resource = pydbe.model.Resource.get_async(id=xxx, _first=True)
            extract_color = resource.extract.customattributemap.get('color')
            extract_annotation = resource.extract.customannotationmap.get('cancer')

    """
    resource_id = 224723 # Please use a resource that exists in your instance
    resource = pydbe.model.Resource.get_async(id=resource_id, _first=True)
    print "Extract id {}, CustomAttribute {}, CustomAnnotation {}".format(resource.extract.id,
                                                                          resource.extract.customattribute,
                                                                          resource.extract.customannotation)
    print "Sample id {}, CustomAttribute {}, CustomAnnotation {}".format(resource.extract.sample.id,
                                                                         resource.extract.sample.customattribute,
                                                                         resource.extract.sample.customannotation)

    print "Take the customattribute value by key 'K1'"
    print resource.extract.sample.customattribute[0]['K1']
    print "Try to take the customattribute value by unknown key 'K10'"
    try:
        print resource.extract.sample.customattribute[0]['K10']
    except KeyError:
        print "got keyerror when accessing unknown key"

    print "Take the customannotation name by type 'Human Phenotype'"
    print resource.extract.sample.customannotation[0]['Human Phenotype']

    print "Take the customattribute from map helper function"
    print resource.extract.sample.customattributemap.get('K1')
    print resource.extract.sample.customannotationmap.get('Human Phenotype')
