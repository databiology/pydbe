#!/usr/bin/env python

## 04_get_entity_example.py
## Example PyDBE script to retrieve information for an entity (Resource, Extract,
## Sample, Episode, Subject, Project, Dataset).
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import pprint
import logging

# DBE pydbe.model package
import pydbe.model

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print """
        Get hierarchical structure of DBE entity,
        e.g. get Resource with id=100, and parent Extract/Sample information
            resources = pydbe.model.Resource.get(id=100)
            for resource in resources:
                print resource.name
                print resource.extract.name
                print resource.extract.sample.name

        Note: be aware of AttributeError could be returned for any Resource with
        no Extract or Sample associated
    """
    resource_id = [100, 101] # Please use an existing resources in your instance
    resources = pydbe.model.Resource.get(id=resource_id)
    for resource in resources:
        print "Resource name {}".format(resource.name)

        if resource.extract:
            print "Extract name {}".format(resource.extract.name)
            if resource.extract.sample:
                print "Sample name {}".format(resource.extract.sample.name)

    print """
        You can use "_first=True" option to retrieve only one result
            entity = pydbe.model.<Entity>.get(...., _first=True)
    """
    resource = pydbe.model.Resource.get(id=resource_id, _first=True)
    print "Resource name {}".format(resource.name)

    print """
        Get an Entity with one option filtering
        e.g. Get Workunits named exactly as "Jupyter"
            workunit = pydbe.model.Workunit.get(name="Jupyter")

        Note: PyDBE only supports exact matches
    """
    wu_name = 'Jupyter' # Please use a Workunit that exists in your instance
    entities = pydbe.model.Workunit.get(name=wu_name)
    for entity in entities:
        print "Workunit id {0}, name {1}".format(entity.id, entity.name)

    print """
        Get an Entity with two options filtering
        e.g. Get Resources which name is 'test.bam' and projectid is '2'

            resources = pydbe.model.Resource.get(name='test.bam', projectid=2)
    """
    resource_name = 'test.bam' # Please use a file that exists in your instance
    project_id = 2 # Please use a project that exists in your instance
    resources = pydbe.model.Resource.get(name=resource_name, projectid=project_id)
    print resources

    print """
        Get Entities with multiple matches for one option
        e.g. Get Resources which name is 'test.bam' and projectid is 2 or 3

            resources = pydbe.model.Resource.get(name='test.bam', projectid=[2, 3])
    """
    project_ids = [2, 3] # Please use a list of projects that exist in your instance
    resources = pydbe.model.Resource.get(name=resource_name, projectid=project_ids)
    print resources

    print """
        Show Entities basic information
    """
    project_id = 5
    prj = pydbe.model.Project.get(id=project_id, _first=True)
    print "Project {} is {}".format(prj.id, prj.name)

    subject_id = 23
    sub = pydbe.model.Subject.get(id=subject_id, _first=True)
    print "Subject {} is {}".format(sub.id, sub.individualid)

    episode_id = 52
    epi = pydbe.model.Episode.get(id=episode_id, _first=True)
    print "Episode {} is {}".format(epi.date, epi.type)

    sample_id = 512
    sam = pydbe.model.Sample.get(id=sample_id, _first=True)
    print "Sample {} is {}".format(sam.id, sam.name)

    extract_id = 3652
    ext = pydbe.model.Extract.get(id=extract_id, _first=True)
    print "Extract {} is {}".format(ext.id, ext.name)

    resource_id = 3533
    res = pydbe.model.Resource.get(id=resource_id, _first=True)
    print "Resource {} is {}".format(res.id, res.name)
