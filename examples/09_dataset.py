#!/usr/bin/env python

## 09_dataset.py
## Example PyDBE script to get Dataset information.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import logging

# DBE pydbe.model package
import pydbe.model

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe").setLevel(logging.DEBUG)
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print '''
        Datasets can be selected by name, id or any other properties
    '''
    dataset_id = 2 # Please use an existing dataset in your instance
    ds = pydbe.model.Dataset.get_async(id=dataset_id, _first=True)

    print '''
        Datasets are retrieved with attribute information, here we just convert
        that information as a table.
    '''
    # Example scriptlet to print dataset in tabular form,
    # !!!CAUTION!!! sorted function is not efficient for large dataset, use it with care
    sorted_attr_names = map(lambda a: a['name'], ds.attribute(sort=True))

    print "Dataset in tabular form"
    # Print dataset header
    print " | ".join(sorted_attr_names)
    # Print dataset rows
    for item in ds.item(item_sort=True, field_sort=True):
        # Each item (row) contains fields (cells)
        field_values = map(lambda f: f['value'], item['field'])
        print " | ".join(field_values)

    # Search a specific type of attribute, e.g. resource type
    print "Get all values from cell with 'resource' type"
    resource_ids = ds.get_value_by_attribute_type('resource')
    print resource_ids
