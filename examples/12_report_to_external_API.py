#!/usr/bin/env python

## 12_report_to_external_API.py
## Example PyDBE script to run Workunits and when they're completed, send a
## notification to a Slack channel.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import logging
from time import sleep

# DBE pydbe.model package
import pydbe.model

# Send notification to Slack API
from slackclient import SlackClient
slack_token = "SLACK_TOKEN"
sc = SlackClient(slack_token)
mychannel = "CHANNEL_ID"

# Global variables
verbose = True
wait = 10 # seconds to wait before checking again
resource_list = [ 101, 102, 103, 104 ]
workunit_id = 523 # use an existing WU in your instance

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe").setLevel(logging.DEBUG)
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    all_wu = []
    total_wu = len(resource_list)

    for res in resource_list:
        # Get information for new resource to analyze
        resource = pydbe.model.Resource.get(id=res)
        # Collect Workunit metadata from a previous run
        workunit = pydbe.model.Workunit.get(id=workunit_id, _first=True)
        # Use new input resources
        inputresourceids = [res]
        # Collect parameters used before
        parameterids = [p.id for p in workunit.parameter]
        # Collect wrapper configuration
        wrappercreator = pydbe.model.WrapperCreator.get(_first=True)

        # Note that we use 'post' to create a new workunit using the same configuration
        rerun_workunit = pydbe.model.Workunit.post(name="Analysis for {}".format(resource.name),
                                                   projectid=workunit.project.id,
                                                   applicationid=workunit.application.id,
                                                   storageid=workunit.storage.id,
                                                   submitterid=workunit.submitter.id,
                                                   wrappercreatorid=wrappercreator.id,
                                                   inputresourceid=inputresourceids,
                                                   parameterid=parameterids)
        if verbose:
            print "New workunit {} launched for {}".format(rerun_workunit.id, resource.name)

    if verbose:
        print "All Workunits were launched"

    # Wait for all WU to finish
    while True:
        ready_wu = 0
        sleep(wait)

        wu_iter =  pydbe.model.Workunit.get_async(id=all_wu)
        for wu in wu_iter:
            if wu.status == "available":
                ready_wu = ready_wu + 1
            elif wu.status == "failed":
                ready_wu = ready_wu + 1
        if ready_wu == total_wu:
            if verbose:
                print "All Workunits are completed"
            break

    wu_iter =  pydbe.model.Workunit.get_async(id=all_wu)
    for wu in wu_iter:
        url = 'https://www.test.databiology.net/dbe/userlab/show-workunit.html?workunitId={}'.format(wu.id)
        message = "{} is {}, link: {}".format(wu.name, wu.staus, url)
        # Send notification to Slack
        sc.api_call(
          "chat.postMessage",
          channel=mychannel,
          text=message
        )
