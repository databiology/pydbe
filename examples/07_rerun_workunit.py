#!/usr/bin/env python

## 07_rerun_workunit.py
## Example PyDBE script to rerun a Workunit using the previous configuration.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.


# Basic Python packages
import os
import logging

# DBE pydbe.model package
import pydbe.model

if __name__ == "__main__":

    logging.basicConfig()
    logging.getLogger("pydbe").setLevel(logging.DEBUG)
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print '''
        To rerun an existing workunit with same parameters, inputs and configuration,
        its needed to collect the workunit metadata. In this example we use a previous
        Workunit, after retrivieng the entity, we collect the inputs in 'inputresourceids',
        the parameters in 'parameterids' and the wrapper configuration in 'wrappercreator'.
        The rest of the configurations are taken from the workunit entity (project, storage,
         application)

    '''
    workunit_id = 581 # Please use an existing Workunit in your instance

    # Collect Workunit metadata
    workunit = pydbe.model.Workunit.get(id=workunit_id, _first=True)
    # Collect input resoures used before
    inputresourceids = [r.id for r in workunit.inputresource]
    # Collect parameters used before
    parameterids = [p.id for p in workunit.parameter]
    # Collect wrapper configuration
    wrappercreator = pydbe.model.WrapperCreator.get(_first=True)

    # Note that we use 'post' to create a new workunit using the same configuration
    rerun_workunit = pydbe.model.Workunit.post(name="Rerun workunit id {} thru api".format(workunit_id),
                                               projectid=workunit.project.id,
                                               applicationid=workunit.application.id,
                                               storageid=workunit.storage.id,
                                               submitterid=workunit.submitter.id,
                                               wrappercreatorid=wrappercreator.id,
                                               inputresourceid=inputresourceids,
                                               parameterid=parameterids)
    # Retrieve new Workunit id
    print "Rerun workunit with id {}".format(rerun_workunit.id)

    # Check for Workunit status
    from time import sleep
    while True:
        wu = pydbe.model.Workunit.get_async(id=rerun_workunit.id, _first=True)
        status = wu.status
        if status == "pending":
            print "Workunit {} is preparing/running".format(wu.id)
            sleep 60
        elif status == "available":
            print "Workunit {} is done".format(wu.id)
            break
        elif status == "failed":
            print "Workunit {} is failed".format(wu.id)
            break
        else:
            print "Unknown status {}".format(status)
            break
