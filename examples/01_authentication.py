#!/usr/bin/env python

## 01_authentication.py
## Example PyDBE script to show authentication modes.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os

# DBE pydbe.model package
import pydbe.model

print """
    Setup DBE Client Token with dbecli (included in PyDBE python package), the command will ask for a password in hidden input
    e.g.
      dbecli login -i https://www.<your_instance>.databiology.net/dbe -u username
       Token filename : /home/user/.dbe/node.jwt
       Password:
      Logging in DBE https://www.<your_instance>.databiology.net/dbe for username
      Logged in DBE, and stored to /home/user/.dbe/node.jwt

    In above example, token was stored in the default location $HOME/.dbe/node.jwt (or %USERPROFILE%\.dbe\node.jwt in MS-Windows)

    If you use the default location for the token file, you can now access Entity from DBE
    e.g. Get Resource with id=100 and id=101
"""
resouces_ids = [100, 101] # Please use a list of resources that exist in your instance
resources = pydbe.model.Resource.get(id=resouces_ids)
print resources

print """
    if you use custom location for the token file e.g use ~/tokens/node_access.jwt instead of the system default
    you have two options
    (Option 1) Use environment variable DBE_TOKEN_FILENAME in Bash or python os.environ['DBE_TOKEN_FILENAME']

    For Bash you can use:
        export DBE_TOKEN_FILENAME=/path/to/node.jwt
        echo $DBE_TOKEN_FILENAME

    For Windows:
        set DBE_TOKEN_FILENAME="%USERPROFILE%\.dbe\node.jwt"
        echo %DBE_TOKEN_FILENAME%

"""
os.environ['DBE_TOKEN_FILENAME'] = os.path.join(os.path.expanduser('~'), 'tokens', 'node_access.jwt')
resources = pydbe.model.Resource.get(id=resouces_ids)
print resources

print """
    (Option 2) Use explicit DBE internal client (usually call dbeclient) in model api
"""
dbeclient = pydbe.DBEClient.from_filename(os.path.join(os.path.expanduser('~'), 'tokens', 'node_access.jwt'))
resources = pydbe.model.Resource.get(id=resouces_ids, dbeclient=dbeclient)
print resources
