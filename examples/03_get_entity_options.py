#!/usr/bin/env python

## 03_get_entity_options.py
## Example PyDBE script to retrieve information for an Entity (Resource, Sample,
## Extract, Project, Dataset).
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import pprint
import logging

# DBE pydbe.model package
import pydbe.model

# python script main func
if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print """
        Pydbe model API provides a set of helper functions, such as
        get_options(), put_options() and get_result_attributes()
    """

    # Use pprint package to "pretty print" the output
    print """
        Resource model API 'get_options()', it shows options for 'get()'
        """
    pprint.pprint(pydbe.model.Resource.get_options())

    print """
        Resource model API 'put_options()', it shows options for 'put()'
        """
    pprint.pprint(pydbe.model.Resource.put_options())

    print """
        Resource model API 'get_result_attributes', it shows what a Resource object contains
        """
    pprint.pprint(pydbe.model.Resource.get_result_attributes())
