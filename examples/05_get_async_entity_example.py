#!/usr/bin/env python

## 05_get_async_entity_example.py
## Example PyDBE script to retrieve information in async mode.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import pprint
import logging

# DBE pydbe.model package
import pydbe.model

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe").setLevel(logging.DEBUG)
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print """
        API 'get' function is handy for development, but in production we should
        use 'get_async' function which provide python 'iterable' object,
        not python list.

        e.g. to get ALL resources from the system (query options = empty)
            resources = pydbe.model.Resource.get_async()

        And iterate one by one
            for resource in resources:
                print resource.name

        After iterated all entries, it can not be reiterate again. below code would print nothing
            for resource in resources:
                print resource.name

        So anytime want to reiterate the same iterable, use the get_async func one more time
    """
    resources = pydbe.model.Resource.get_async()
    for resource in resources:
        print resource.name
    print "Reiterate again"
    for resource in resources:
        print resource.name
    print "Nothing get printed"
