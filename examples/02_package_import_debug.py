#!/usr/bin/env python

## 02_package_import_debug.py
## Example PyDBE script to show debug logging modes.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import logging

# DBE pydbe.model package - DBE Entity Model Abstraction Layer
import pydbe.model

# Enable pydbe and pydbe.model debug log
logging.basicConfig()
logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

print """
    With import and logging config correctly, you can now start exploring pydbe
        import logging
        import pydbe.model

        logging.basicConfig()
        logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

"""
