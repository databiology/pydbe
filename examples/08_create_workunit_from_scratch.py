#!/usr/bin/env python

## 08_create_workunit_from_scratch.py
## Example PyDBE script to create a Workunit using a new configuration.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import logging

# DBE pydbe.model package
import pydbe.model

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    print """
        In order to run analysis workunit, we need to gather the followings
            1) Which Project the workunit belongs to
            2) What to run, Application
            3) Where to run, Submitter
            4) Which input you need to add,
                a) Input Resource
                b) Input Dataset
            5) Where the output goes to, Storage
    """

    print """
        Get project id 2
        project_2 = pydbe.model.Project.get(id=2, _first=True)
    """
    project_2 = pydbe.model.Project.get(id=2, _first=True)
    print project_2.name

    print """
        Get application with name 'FastQC'
        app = pydbe.model.Application.get(name='FastQC', _first=True)
    """
    app_fastqc = pydbe.model.Application.get(name='FastQC', _first=True)
    print app_fastqc.name

    print """
        Get first writable Storage
    """
    storages = pydbe.model.Storage.get()
    # no server side filter by readonly, As API does not support filter out readonly, we use client side filtering
    storage_writable = None
    for storage in storages:
        if not storage.readonly:
            storage_writable = storage
    if not storage_writable:
        raise Exception("Please contact an operator to create a storage")
    print storage_writable

    print """
        Get submitter with name contains bluemix/ibm
    """
    submitters = pydbe.model.Submitter.get_async()
    submitters_bluemix = [submitter for submitter in submitters if
                          submitter.name.lower().find('bluemix') > 0 or submitter.name.lower().find('ibm') > 0]
    if not submitters_bluemix:
        raise Exception("No bluemix/ibm submitter")
    submitter = submitters_bluemix[0]
    print submitter.name

    print """
        Get Wrapper configuration
    """
    wrapper = pydbe.model.WrapperCreator.get(_first=True)

    print """
        Add resources, those are refered by resourceid
    """
    inputresourcesids = [100]

    print """
        Adding submitter parameters, note that the 'profile' value is changed.
    """
    parameterids = []
    for p in pydbe.model.Parameter.get(submitterid=submitter._id):
        if p.key == 'profile':
            parameter_details = {'value': 'small', 'context': p.context, 'description': p.description,
                                 'label': p.label, 'key': p.key, 'type': p.type, 'required': p.required,
                                 'modifiable': p.modifiable, 'choices': p.choices}
            new_p = pydbe.model.Parameter.put(**parameter_details)
            print "Newly created parameter, {}".format(new_p)
            parameterids.append(new_p.id)

    print '''
        Create the new workunit using 'post'
    '''
    workunit = pydbe.model.Workunit.post(name="Run {} thru pydbe".format(app_fastqc.name),
                                         projectid=project_2.id,
                                         applicationid=app_fastqc._id,
                                         storageid=storage_writable._id,
                                         submitterid=submitter._id,
                                         wrappercreatorid=wrapper.id,
                                         parameterid=parameterids,
                                         inputresourceid=inputresourcesids)

    print '''
        Show the new workunit id
    '''
    print "Created workunit with id {}".format(workunit.id)
