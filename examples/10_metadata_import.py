#!/usr/bin/env python

## 10_metadata_import.py
## Example PyDBE script to import metadata information.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import csv
import logging
import tempfile

# DBE pydbe.model package
import pydbe.model

print '''
    In this example a Resource metadata is extended with key:values
    pairs with 'name':'resource1' and 'filename':'somewhere/resource1.txt'
'''
metadata = [
    ['Resource', 'Resource'],
    ['name', 'filename'],
    ['resource1', 'somewhere/resource1.txt']
]


def write_temp_csv():
    print '''
        Create a CSV temporal file with metadata information, it contains:

        "Resource","Resource"
        "name","filename"
        "resource1","somewhere/resource1.txt"

    '''
    tempfp = tempfile.NamedTemporaryFile(delete=False)
    writer = csv.writer(tempfp)
    for line in metadata:
        writer.writerow(line)
    tempfp.close()
    return tempfp.name


if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    # Define the enviromental variable DBE_TOKEN_FILENAME with token ~/.dbe/node.jwt
    os.environ['DBE_TOKEN_FILENAME'] = os.path.join(os.path.expanduser('~'), '.dbe', 'node.jwt')

    # Create a dbeclient interface
    dbeclient = pydbe.DBEClient.from_default()

    temp_metadata_filename = write_temp_csv()
    try:
        print '''
            Upload metadata file to Project 1 in Workunit 10

            taskinfo = dbeclient.metadata.upload_metadata(project_id=1, workunit_id=10,
                                                          metadata_filename=temp_metadata_filename)

        '''
        taskinfo = dbeclient.metadata.upload_metadata(project_id=1, workunit_id=10,
                                                      metadata_filename=temp_metadata_filename)
        print '''
            Check status of the upload in workunit 10 with
                dbeclient.metadata.get_metadata_upload_status(workunit_id=10, taskname=taskinfo['taskname'])
        '''
        print dbeclient.metadata.get_metadata_upload_status(workunit_id=10, taskname=taskinfo['taskname'])
    finally:
        # Delete temporal file
        if os.path.exists(temp_metadata_filename):
            os.unlink(temp_metadata_filename)
