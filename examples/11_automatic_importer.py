#!/usr/bin/env python

## 11_automatic_importer.py
## Example PyDBE script to run an import Workunit when a new file is added in a
## monitored directoy in a Storage.
## Copyright (C) 2013-2018 Databiology Ltd.
## All Rights Reserved.

# Basic Python packages
import os
import logging
import boto3 # boto expects S3 credentials stored in ~/.aws/credentials
from time import sleep

# DBE pydbe.model package
import pydbe.model

# Global variables
verbose = True
wait = 10 # seconds to wait before checking again

# location to monitor, we monitor a S3 bucket checking if a file named
# "ready.txt" exist in the directory
s3 = boto3.resource('s3')
BUCKET = "my_bucket_for_data"
TRIGGER = "ready.txt"

# Data directories to watch
DIRS = { "dir1" : "dir1/data/", "dir2" : "dir2/data/", "dir3" : "dir3/data/" }

def launchWU(name, dir):
    # Run ResourceImporter app to import files inside "dir"
    workunit_name = "Import for {}".format(name)
    project_id = 5
    wrapper_id = 2
    submitter_id = 2
    importstorage_id = 23
    storage_id = 24
    app_id = 12

    # Get App parameters and set "rlocation" to "dir"
    parameterids = []
    for p in pydbe.model.Parameter.get_async(applicationid=application_id):
        if p.key == 'rlocation':
            parameter_details = {'value': dir,
                                 'context': p.context,
                                 'description': p.description,
                                 'label': p.label,
                                 'key': p.key,
                                 'type': p.type,
                                 'required': p.required,
                                 'modifiable': p.modifiable}
            new_p = pydbe.model.Parameter.put(**parameter_details)
            parameterids.append(new_p.id)
        else: # just add the rest of the parameters
            parameterids.append(p.id)

    # Launch the new workunit
    workunit = pydbe.model.Workunit.post(name=workunit_name,
                                         projectid=project_id,
                                         applicationid=application_id,
                                         importstorageid=importstorage_id,
                                         storageid=storage_id,
                                         submitterid=submitter_id,
                                         wrappercreatorid=wrapper_id,
                                         parameterid=parameterids)
    return workunit.id

if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger("pydbe").setLevel(logging.DEBUG)
    logging.getLogger("pydbe.model").setLevel(logging.DEBUG)

    # Start the monitor
    all_wu = []
    total_wu = len(DIRS)
    READY = {}
    while True:
        num_wu = 0
        for imp in DIRS:
            if imp in READY: # Verify if the directory has been imported
                num_wu = num_wu + 1
                continue

            # Define KEY for file in bucket
            dir = DIRS[imp]
            KEY = dir + TRIGGER
            obj = s3.Object(BUCKET, KEY)
            if verbose:
                print "Checking for {} in bucket {}".format(KEY, BUCKET)

            # Check if the KEY exist in the BUCKET
            try:
                t = obj.get()
                if verbose:
                    print "Trigger file present, starting import"
                new_wu = lauchWU(imp, dir)
                if verbose:
                    print "Created new WU {}".format(new_wu)
                all_wu.append(new_wu)
                READY[imp] = True
                sleep(wait)
            except:
                if verbose:
                    print "Directory '{}' is not ready".format(imp)
                sleep(wait)

        # Check if all Workunits has been processed
        if num_wu == total_wu:
            if verbose:
                print "All WU imports were launched"
            break

    # Wait for all WU to finish
    while True:
        ready_wu = 0
        wu_iter = pydbe.model.Workunit.get_async(id=all_wu)
        for wu in wu_iter:
            if wu.status == "available":
                ready_wu = ready_wu + 1
                if verbose:
                    print "Workunit {} is complete".format(wu.id)
            elif wu.status == "failed":
                ready_wu = ready_wu + 1
                if verbose:
                    print "Workunit {} is failed".format(wu.id)
            else:
                if verbose:
                    print "Workunit {} is still {}".format(wu.id, wu.status)
        if ready_wu == total_wu:
            if verbose:
                print "All Workunits are completed"
            break
